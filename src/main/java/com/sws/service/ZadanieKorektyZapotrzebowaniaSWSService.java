package com.sws.service;

import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;
import pl.nbp.sws.pmcorrectionrequest.model.PmCorrectionRequestInMessage;
import pl.nbp.sws.pmcorrectionrequest.model.PmCorrectionRequestOutMessage;
import pl.nbp.sws.pmcorrectionrequest.model.Request;
import pl.nbp.sws.pmcorrectionrequest.model.Response;

@Service
public class ZadanieKorektyZapotrzebowaniaSWSService {

    public PmCorrectionRequestOutMessage prepareResponse(PmCorrectionRequestInMessage request) {
        Request req = request.getRequest();
        Response res = new Response();

        if (Strings.isBlank(req.getIdPobraniaSWS()) || req.getIdDokumentuEOD() == 0) {
            res.setKodPowrotu("-1");
            res.setKomunikatBledu("ZadanieKorektyZapotrzebowaniaSWS.komunikatBledu: Wymagane pole nie jest ustawione.");
        } else {
            res.setIdDokumentuEOD(res.getIdDokumentuEOD());
            res.setKodPowrotu("1");
        }

        PmCorrectionRequestOutMessage response = new PmCorrectionRequestOutMessage();
        response.setResponse(res);
        return response;
    }
}
