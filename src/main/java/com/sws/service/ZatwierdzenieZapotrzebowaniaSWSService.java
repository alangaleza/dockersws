package com.sws.service;

import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;
import pl.nbp.sws.pmacceptance.model.PmAcceptanceInMessage;
import pl.nbp.sws.pmacceptance.model.PmAcceptanceOutMessage;
import pl.nbp.sws.pmacceptance.model.Request;
import pl.nbp.sws.pmacceptance.model.Response;

@Service
public class ZatwierdzenieZapotrzebowaniaSWSService {

    public PmAcceptanceOutMessage prepareResponse(PmAcceptanceInMessage request) {
        Request req = request.getRequest();
        Response res = new Response();

        if (Strings.isBlank(req.getIdPobraniaSWS()) || req.getIdDokumentuEod() == 0) {
            res.setKodPowrotu("-1");
            res.setKomunikatBledu("ZatwierdzenieZapotrzebowaniaSWS.komunikatBledu: Wymagane pole nie jest ustawione.");
        } else {
            res.setIdDokumentuEod(res.getIdDokumentuEod());
            res.setKodPowrotu("1");
        }

        PmAcceptanceOutMessage response = new PmAcceptanceOutMessage();
        response.setResponse(res);
        return response;
    }
}
