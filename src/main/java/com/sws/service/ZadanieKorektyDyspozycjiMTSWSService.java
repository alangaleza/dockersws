package com.sws.service;

import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;
import pl.nbp.sws.odmtcorrectionrequest.model.OdMtCorrectionRequestInMessage;
import pl.nbp.sws.odmtcorrectionrequest.model.OdMtCorrectionRequestOutMessage;
import pl.nbp.sws.odmtcorrectionrequest.model.Request;
import pl.nbp.sws.odmtcorrectionrequest.model.Response;

@Service
public class ZadanieKorektyDyspozycjiMTSWSService {

    public OdMtCorrectionRequestOutMessage prepareResponse(OdMtCorrectionRequestInMessage request) {
        Request req = request.getRequest();
        Response res = new Response();

        if (Strings.isBlank(req.getIdOdMtSWS()) || req.getIdDokumentuEod() == 0) {
            res.setKodPowrotu("-1");
            res.setKomunikatBledu("ZadanieKorektyZapotrzebowaniaSWS.komunikatBledu: Wymagane pole nie jest ustawione.");
        } else {
            res.setIdDokumentuEod(res.getIdDokumentuEod());
            res.setKodPowrotu("1");
        }

        OdMtCorrectionRequestOutMessage response = new OdMtCorrectionRequestOutMessage();
        response.setResponse(res);
        return response;
    }
}
