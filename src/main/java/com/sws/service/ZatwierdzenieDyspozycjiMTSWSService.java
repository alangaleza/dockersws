package com.sws.service;

import org.springframework.stereotype.Service;
import pl.nbp.sws.odmtacceptance.model.OdMtAcceptanceInMessage;
import pl.nbp.sws.odmtacceptance.model.OdMtAcceptanceOutMessage;
import pl.nbp.sws.odmtacceptance.model.Request;
import pl.nbp.sws.odmtacceptance.model.Response;

@Service
public class ZatwierdzenieDyspozycjiMTSWSService {

    public OdMtAcceptanceOutMessage prepareResponse(OdMtAcceptanceInMessage request) {
        Request req = request.getRequest();
        Response res = new Response();

        if (req.getIdOdMtSWS() == null || req.getIdDokumentuEod() == 0) {
            res.setKodPowrotu("-1");
            res.setKomunikatBledu("ZatwierdzenieDyspozycjiMTSWS.komunikatBledu: Wymagane pole nie jest ustawione.");
        } else {
            res.setIdDokumentuEod(res.getIdDokumentuEod());
            res.setKodPowrotu("1");
        }

        OdMtAcceptanceOutMessage response = new OdMtAcceptanceOutMessage();
        response.setResponse(res);
        return response;
    }
}
