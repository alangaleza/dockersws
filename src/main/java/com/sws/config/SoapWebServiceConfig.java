package com.sws.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
public class SoapWebServiceConfig extends WsConfigurerAdapter {

    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext context) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(context);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(servlet, "/ws/*");
    }

    @Bean(name = "reqMTSWSCorrectionDemand")
    public XsdSchema reqMTSWSCorrectionDemand() {
        return new SimpleXsdSchema(new ClassPathResource("PmCorrectionRequest.xsd"));
    }

    @Bean(name = "dispMTSWSCorrectionDemand")
    public XsdSchema dispMTSWSDemandSchema() {
        return new SimpleXsdSchema(new ClassPathResource("OdMtCorrectionRequest.xsd"));
    }

    @Bean(name = "reqSWSApproval")
    public XsdSchema reqSWSApproval() {
        return new SimpleXsdSchema(new ClassPathResource("PmAcceptance.xsd"));
    }

    @Bean(name = "dyspMTSWSApproval")
    public XsdSchema dyspMTSWSApproval() {
        return new SimpleXsdSchema(new ClassPathResource("OdMtAcceptance.xsd"));
    }

    @Bean(name = "PmCorrectionRequest")
    public DefaultWsdl11Definition zadanieKorektyZapotrzebowaniaSWS(@Qualifier(value = "reqMTSWSCorrectionDemand") XsdSchema reqMTSWSCorrectionDemand) {

        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setSchema(reqMTSWSCorrectionDemand);
        definition.setLocationUri("/ws");
        definition.setPortTypeName("PmCorrectionRequest");
        definition.setTargetNamespace("http://www.nbp.pl/sws/PmCorrectionRequest/model");
        return definition;
    }

    @Bean(name = "OdMtCorrectionRequest")
    public DefaultWsdl11Definition zadanieKorektyDyspozycjiMTSWS(@Qualifier(value = "dispMTSWSCorrectionDemand") XsdSchema dispMTSWSDemandSchema) {

        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setSchema(dispMTSWSDemandSchema);
        definition.setLocationUri("/ws");
        definition.setPortTypeName("OdMtCorrectionRequest");
        definition.setTargetNamespace("http://nbp.pl/sws/OdMtCorrectionRequest/model");
        return definition;
    }

    @Bean(name = "acceptPm")
    public DefaultWsdl11Definition zatwierdzenieZapotrzebowaniaSWS(@Qualifier(value = "reqSWSApproval") XsdSchema dispMTSWSDemandSchema) {

        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setSchema(dispMTSWSDemandSchema);
        definition.setLocationUri("/ws");
        definition.setPortTypeName("AcceptPm");
        definition.setTargetNamespace("http://nbp.pl/sws/PmAcceptance/model");
        return definition;
    }

    @Bean(name = "OdMtAcceptance")
    public DefaultWsdl11Definition zatwierdzenieDyspozycjiMTSWS(@Qualifier(value = "dyspMTSWSApproval") XsdSchema dispMTSWSDemandSchema) {

        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setSchema(dispMTSWSDemandSchema);
        definition.setLocationUri("/ws");
        definition.setPortTypeName("OdMtAcceptance");
        definition.setTargetNamespace("http://nbp.pl/sws/OdMtAcceptance/model");
        return definition;
    }
}
