package com.sws.endpoint;

import com.sws.service.ZatwierdzenieDyspozycjiMTSWSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import pl.nbp.sws.odmtacceptance.model.OdMtAcceptanceInMessage;
import pl.nbp.sws.odmtacceptance.model.OdMtAcceptanceOutMessage;

@Endpoint
public class ZatwierdzenieDyspozycjiMTSWSEndpoint {

    @Autowired
    private ZatwierdzenieDyspozycjiMTSWSService swsService;

    @PayloadRoot(namespace = "http://nbp.pl/sws/OdMtAcceptance/model", localPart = "OdMtAcceptanceInMessage")
    @ResponsePayload
    public OdMtAcceptanceOutMessage getRequest(@RequestPayload OdMtAcceptanceInMessage request) {
        OdMtAcceptanceOutMessage response = swsService.prepareResponse(request);
        return response;
    }
}
