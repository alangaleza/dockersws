package com.sws.endpoint;

import com.sws.service.ZadanieKorektyDyspozycjiMTSWSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import pl.nbp.sws.odmtcorrectionrequest.model.OdMtCorrectionRequestInMessage;
import pl.nbp.sws.odmtcorrectionrequest.model.OdMtCorrectionRequestOutMessage;

@Endpoint
public class ZadanieKorektyDyspozycjiMTSWSEndpoint {

    @Autowired
    private ZadanieKorektyDyspozycjiMTSWSService swsService;

    @PayloadRoot(namespace = "http://nbp.pl/sws/OdMtCorrectionRequest/model", localPart = "odMtCorrectionRequestInMessage")
    @ResponsePayload
    public OdMtCorrectionRequestOutMessage getRequest(@RequestPayload OdMtCorrectionRequestInMessage request) {
        OdMtCorrectionRequestOutMessage response = swsService.prepareResponse(request);
        return response;
    }
}
