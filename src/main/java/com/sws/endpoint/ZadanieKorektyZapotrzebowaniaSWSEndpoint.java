package com.sws.endpoint;

import com.sws.service.ZadanieKorektyZapotrzebowaniaSWSService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import pl.nbp.sws.pmcorrectionrequest.model.PmCorrectionRequestInMessage;
import pl.nbp.sws.pmcorrectionrequest.model.PmCorrectionRequestOutMessage;

@Endpoint
public class ZadanieKorektyZapotrzebowaniaSWSEndpoint {

    @Autowired
    private ZadanieKorektyZapotrzebowaniaSWSService swsService;

    @PayloadRoot(namespace = "http://nbp.pl/sws/PmCorrectionRequest/model", localPart = "pmCorrectionRequestInMessage")
    @ResponsePayload
    public PmCorrectionRequestOutMessage getRequest(@RequestPayload PmCorrectionRequestInMessage request) {
        PmCorrectionRequestOutMessage response = swsService.prepareResponse(request);
        return response;
    }
}
