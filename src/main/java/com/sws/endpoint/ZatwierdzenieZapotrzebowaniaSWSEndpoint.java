package com.sws.endpoint;

import com.sws.service.ZatwierdzenieZapotrzebowaniaSWSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import pl.nbp.sws.pmacceptance.model.PmAcceptanceInMessage;
import pl.nbp.sws.pmacceptance.model.PmAcceptanceOutMessage;

@Endpoint
public class ZatwierdzenieZapotrzebowaniaSWSEndpoint {

    @Autowired
    private ZatwierdzenieZapotrzebowaniaSWSService swsService;

    @PayloadRoot(namespace = "http://nbp.pl/sws/PmAcceptance/model", localPart = "PmAcceptanceInMessage")
    @ResponsePayload
    public PmAcceptanceOutMessage getRequest(@RequestPayload PmAcceptanceInMessage request) {
        PmAcceptanceOutMessage response = swsService.prepareResponse(request);
        return response;
    }
}
